# DCL Elevator Class

This is an elevator class to be used in DCL. You can view this demo scene to play with all the features, and then follow the installation instructions to use the elevator in your projects. You can supply your own elevator GLTF, use the one packaged here, or leave blank and use a Plane Shape.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You will need to have downloaded the utils library from NPM

```javascript
$npm i decentraland-ecs-utils
```

And make sure in your package.json file you have the "latest" for the version

```javascript
  "dependencies": {
    "decentraland-ecs-utils": "latest"
  }
```

And make sure to import the utils library in your main game.ts file

```javascript
import utils from "node_modules/decentraland-ecs-utils/index"
```

## Installing

1 - Download this entire project

2 - Copy the elevator.ts file into your /src folder where your main game.ts file resides. 

3 - Copy all the models into your /models folder (you don't have to use the elevator.glb file, but the others contain colliders and MUST be inside your /models folder

4 - Copy the sounds into your /sounds folder (these aren't required, but you can use them if so)

### Coding the elevator into your scene

1 - Make sure the **elevator.ts** file is imported into your scene

```javascript
import * as Elevators from "./elevator"
```

2 - Make sure you have a trigger component for your avatar (this code has been supplied by DCL in their utils Github)
```javascript
utils.TriggerSystem.instance.setCameraTriggerShape(new utils.TriggerBoxShape(new Vector3(0.5, 1.8, 0.5), new Vector3(0, -0.91, 0)))
```

3 - If you don't have an events manager, create one inside your **game.ts** file
```javascript
const events = new EventManager()
```

4 - Create a new elevator class inside your **game.ts** file

```javascript
  elevator = new Elevators.Elevator(
  events, //handle the events of the elevator
  false, //start the elevator in the air
  new Vector3(8,.3,8), //ground position
  new Vector3(8,13.5,8), //floating position
  new Vector3(.95,.95,.95), //elevator scale
  Quaternion.Euler(0,0,0), //elevator rotation within the scene
  true, //start the elevator opened
  5, //elevator speed in seconds
  'models/elevator.glb', //elevator GLTF shape location - set to null for a blank Plane Shape
  'models/HAL9000_02.glb', //elevator button shape location - set to null for a blank Plane Shape button
  'sounds/ascending.mp3', //location of descending audio - set to null for no audio
  'sounds/descending.mp3', //location of ascending audio - set to null for no audio
  false //show elevator colliders
  )
elevator.setParent(scene)
```

5 - Add elevator event listeners to your code so you can do things based on the state of the elevator

```javascript
///Elevator event listeners
events.addListener(Elevators.ClosedElevator,null,()=>{
  //log("user closed door, time to move in the right direction based on current position")
  elevator.floating ? elevator.descendElevator() : elevator.liftElevator()
})

events.addListener(Elevators.OpenedElevator,null,()=>{
  //log("user open door")
})

events.addListener(Elevators.MovedElevator,null,()=>{
  //log("eleveator moved, open doors")
  elevator.openElevator()
})
```

Boom! You now have a working elevator in your scene :)

More updates to come!

## Sample Game.ts file

```javascript
import utils from "node_modules/decentraland-ecs-utils/index"
import * as Elevators from "./elevator"

utils.TriggerSystem.instance.setCameraTriggerShape(new utils.TriggerBoxShape(new Vector3(0.5, 1.8, 0.5), new Vector3(0, -0.91, 0)))

const scene = new Entity("lastraum-dclu-elevator")
const scenetransform = new Transform({ position: new Vector3(0, 0, 0), rotation: Quaternion.Euler(0, 0, 0), scale: new Vector3(1, 1, 1) })
scene.addComponent(scenetransform)
engine.addEntity(scene)
const events = new EventManager()

let elevator = new Elevators.Elevator(
  events, //handle the events of the elevator
  false, //start the elevator in the air
  new Vector3(8,.3,8), //ground position
  new Vector3(8,13.5,8), //floating position
  new Vector3(.95,.95,.95), //elevator scale
  Quaternion.Euler(0,0,0), //elevator rotation within the scene
  true, //start the elevator opened
  5, //elevator speed
  'models/elevator.glb', //elevator GLTF shape location - set to null for a blank Plane Shape
  'models/HAL9000_02.glb', //elevator button shape location - set to null for a blank Plane Shape button
  'sounds/ascending.mp3', //location of descending audio - set to null for no audio
  'sounds/descending.mp3', //location of ascending audio - set to null for no audio
  false //show elevator colliders
  )
elevator.setParent(scene)

///Elevator event listeners
events.addListener(Elevators.ClosedElevator,null,()=>{
  //log("user closed door, time to move in the right direction based on current position")
  elevator.floating ? elevator.descendElevator() : elevator.liftElevator()
})

events.addListener(Elevators.OpenedElevator,null,()=>{
  //log("user open door")
})

events.addListener(Elevators.MovedElevator,null,()=>{
  //log("eleveator moved, open doors")
  elevator.openElevator()
})
```

## Screenshots

#### The default GLTF elevator shape
![](https://gitlab.com/lastraumonline/dcl-elevator/raw/master/screenshots/elevator.png)

### Elevator not showing the default GLTF but showing the colliders
*These colliders keep the avatar from leaving the elevator while it's moving*
![](https://gitlab.com/lastraumonline/dcl-elevator/raw/master/screenshots/elevator_colliders.png)

#### The default GLTF elevator shape hidden, no colliders, only a PlaneShape
![](https://gitlab.com/lastraumonline/dcl-elevator/raw/master/screenshots/elevator_planeshape.png)

## Authors

* **Lastraum Kostanzioso** - *Initial work* - [Lastraum](https://github.com/lastraum)

## License

This project is licensed under the MIT License

## Acknowledgments

* DCL sci fi pack
