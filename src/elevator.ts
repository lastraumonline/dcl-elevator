/////////////////////////////////////////////
//
//Elevator Class v 1.0.1
//
//Written by Lastraum K
//lastraumonline@gmail.com
//
/////////////////////////////////////////////


import utils from "node_modules/decentraland-ecs-utils/index"
import { RotateTransformComponent } from "../node_modules/decentraland-ecs-utils/transform/component/rotate"

@EventConstructor()
export class ClosedElevator{
  constructor() {}
}

@EventConstructor()
export class OpenedElevator{
  constructor() {}
}

@EventConstructor()
export class MovedElevator{
  constructor() {}
}

export class Elevator extends Entity{

    button:Entity
    doorCollider:Entity
    colliderHolder:Entity

    pressed:boolean = false
    moving:boolean = false
    opened:boolean
    floating:boolean
    showColliders:boolean
    inside:boolean = false
    downAudio:boolean
    upAudio:boolean

    floatPosition:Vector3
    groundPosition:Vector3
    scale:Vector3

    speed:number

    descendClip:AudioClip
    ascendClip:AudioClip
    descendAudioSource:AudioSource
    ascendAudioSource:AudioSource

    closedPosition:Vector3 = Vector3.One()
    openPosition:Vector3 = Vector3.Zero()
    
    openedGlassRot:Quaternion = Quaternion.Euler(0,90,0)
    closedGlassRot:Quaternion = Quaternion.Euler(0,0,0)
    rotation:Quaternion

    events:EventManager

constructor(events:EventManager, startFloating:boolean, groundPosition:Vector3, floatingPosition:Vector3, scale:Vector3, rotation:Quaternion, startOpened:boolean, speed:number, shape:string, buttonShape:string, downAudio:string, upAudio:string, showColliders:boolean)
{
    super("ElevatorE")
    this.floating = startFloating
    this.groundPosition = groundPosition
    this.floatPosition = floatingPosition
    this.scale = scale
    this.rotation = rotation
    this.opened = startOpened
    this.speed = speed

    this.descendClip = new AudioClip(downAudio)
    downAudio == null ? this.downAudio = false : this.downAudio = true
    this.descendAudioSource = new AudioSource(this.descendClip)

    this.ascendClip = new AudioClip(upAudio)
    upAudio == null ? this.upAudio = false : this.upAudio = true
    this.ascendAudioSource = new AudioSource(this.ascendClip)

    this.showColliders = showColliders
    this.events = events

    if(shape == null)
    {
      var planeshape = new Entity()
      planeshape.addComponentOrReplace(new PlaneShape())
      planeshape.addComponentOrReplace(new Transform({
        position: new Vector3(0,0,0),
        scale: new Vector3(1.7,1.7,1.7),
        rotation: Quaternion.Euler(90,0,0)
      }))
      planeshape.setParent(this)
    }

    this.addComponentOrReplace(showColliders ? new GLTFShape('models/elevator_colliders.glb') : new GLTFShape(shape))

    this.addComponentOrReplace(new Transform({
      position: this.floating ? this.floatPosition : this.groundPosition,
      scale:this.scale,
      rotation: this.rotation
    }))

    this.doorCollider = new Entity()
    this.doorCollider.addComponent(showColliders ? new GLTFShape('models/doorCollider_show.glb') : new GLTFShape("models/doorCollider.glb"))
    this.doorCollider.addComponent(new Transform({
    position:Vector3.Zero(),
    scale: this.opened ? this.openPosition : this.closedPosition
    }))
    this.doorCollider.setParent(this)

    this.button = new Entity("ElevatorB")
    this.button.addComponentOrReplace(buttonShape == null ? new BoxShape() : new GLTFShape(buttonShape))
    this.button.addComponentOrReplace(new Transform({
        position: new Vector3(-.62,1.3,0.355),
        rotation: Quaternion.Euler(0,123,0),
        scale: new Vector3(.7, .7, .7)
      }))
    this.button.addComponent(new OnClick(e=>{
        this.prepareElevator()
    }))
    this.button.setParent(this)

    this.addComponentOrReplace(new utils.TriggerComponent(new utils.TriggerBoxShape(new Vector3(1,.1,1),new Vector3(0,1,0)),0,null,null,null,()=>{
      //log("inside")
        this.inside = true
      },()=>{
        //log("outside")
        this.inside = false
      },false))
}

prepareElevator()
{
  //log('preparing eleavtor')
  if(this.inside && !this.moving)
  {
    this.closeElevator()
  }
}

openElevator()
{
  //log("done rotating glass, now we can enter / leave")
  this.doorCollider.getComponent(Transform).scale = this.openPosition
  this.opened = true
  this.events.fireEvent(new OpenedElevator())
}

closeElevator()
{
  //log('need to close elevar so we can move')
  this.doorCollider.getComponent(Transform).scale = this.closedPosition
  this.events.fireEvent(new ClosedElevator())
}

descendElevator()
{
  this.downAudio ? this.playAudio(this.descendAudioSource) : ""
  this.moving = true
  this.addComponentOrReplace(new utils.MoveTransformComponent(this.floatPosition, this.groundPosition, this.speed,()=>{
    //log("done descending elevator") 
    this.moving = false
    this.floating = false
    this.events.fireEvent(new MovedElevator())
  }))
}

liftElevator()
{
  this.upAudio ? this.playAudio(this.ascendAudioSource) : ""
  this.moving = true
  this.addComponentOrReplace(new utils.MoveTransformComponent(this.groundPosition, this.floatPosition, this.speed,()=>{
    //log("done lifting elevator")
    this.moving = false
    this.floating = true
    this.events.fireEvent(new MovedElevator())
  }))
}

playAudio(audio:AudioSource)
{
  this.addComponentOrReplace(audio)
  audio.volume = 1
  audio.playOnce()
}

distance(pos1: Vector3, pos2: Vector3): number {
  const a = pos1.x - pos2.x
  const b = pos1.z - pos2.z
  return a * a + b * b
}

}