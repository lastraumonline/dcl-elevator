import utils from "node_modules/decentraland-ecs-utils/index"
import * as Elevators from "./elevator"

utils.TriggerSystem.instance.setCameraTriggerShape(new utils.TriggerBoxShape(new Vector3(0.5, 1.8, 0.5), new Vector3(0, -0.91, 0)))

const scene = new Entity("lastraum-dclu-elevator")
const scenetransform = new Transform({ position: new Vector3(0, 0, 0), rotation: Quaternion.Euler(0, 0, 0), scale: new Vector3(1, 1, 1) })
scene.addComponent(scenetransform)
engine.addEntity(scene)
const events = new EventManager()

let elevator = new Elevators.Elevator(
  events, //handle the events of the elevator
  false, //start the elevator in the air
  new Vector3(8,.3,8), //ground position
  new Vector3(8,13.5,8), //floating position
  new Vector3(.95,.95,.95), //elevator scale
  Quaternion.Euler(0,0,0), //elevator rotation within the scene
  true, //start the elevator opened
  5, //elevator speed
  'models/elevator.glb', //elevator GLTF shape location - set to null for a blank Plane Shape
  'models/HAL9000_02.glb', //elevator button shape location - set to null for a blank Plane Shape button
  'sounds/ascending.mp3', //location of descending audio - set to null for no audio
  'sounds/descending.mp3', //location of ascending audio - set to null for no audio
  false //show elevator colliders
  )
elevator.setParent(scene)

///Elevator event listeners
events.addListener(Elevators.ClosedElevator,null,()=>{
  //log("user closed door, time to move in the right direction based on current position")
  elevator.floating ? elevator.descendElevator() : elevator.liftElevator()
})

events.addListener(Elevators.OpenedElevator,null,()=>{
  //log("user open door")
})

events.addListener(Elevators.MovedElevator,null,()=>{
  //log("eleveator moved, open doors")
  elevator.openElevator()
})